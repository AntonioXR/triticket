var ViewModel = function () {
    var inst = this
    inst.eventosBase = ko.observable()
    inst.eventosPromo = ko.observable()
    inst.tipoEventos = ko.observable()
    inst.BoletaSE = {
        NombreComprador: ko.observable(),
        NIT: ko.observable(),
        VentaType: 1,
        TotalVenta: ko.observable(),
        EventoId: ko.observable(),
        NombreEvento: ko.observable(),
        Precio: ko.observable(),
        CantidadVendidos: ko.observable()
    }
    //Funciones para Botones
    inst.cargarEvento = function () {
        var item = buscarEvento($("#idEventCO").val());
        console.log(item)
        inst.BoletaSE.EventoId(item.EventoId)
        inst.BoletaSE.NombreEvento(item.NombreEvento)
        inst.BoletaSE.Precio(item.Precio)
    }
    inst.USS = {
        UsuarioId: ko.observable(),
        Nombre: ko.observable(),
        Nick: ko.observable(),
        Contrasena: ko.observable(),
        Edad: ko.observable(),
        Telefono: ko.observable(),
        Email: ko.observable(),
        NoTarjeta: ko.observable(),
        RolId: 2
    }
    inst.registrarUS = function () {
        var data = {
            Nombre: inst.USS.Nombre(),
            Nick: inst.USS.Nick(),
            Contrasena: inst.USS.Contrasena(),
            Edad: inst.USS.Edad(),
            Telefono: inst.USS.Telefono(),
            Email: inst.USS.Email(),
            NoTarjeta: inst.USS.NoTarjeta(),
            RolId: 2
        }
        AjaxCall("http://localhost:51978/api/Usuario","post", data).done(function () {
            $('#closeUSAG').click();
            $('#mostrarALERT').html('<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Genial c:</strong> te has <b>Registrado</b> exisotamente </div>')

        })
    }
    inst.venta = 0;
    inst.GenerarVenta = function (formElement) {
        var total = inst.BoletaSE.CantidadVendidos() * inst.BoletaSE.Precio()
        var Boleta = {
            NombreComprador: inst.BoletaSE.NombreComprador(),
            NIT: inst.BoletaSE.NIT(),
            VentaType: 1,
            TotalVenta: total
        }
        AjaxCall("http://localhost:51978/api/Venta", "post", Boleta).done(function (response) {
            for (var i = 0; i < inst.BoletaSE.CantidadVendidos() ; i++) {
                var Datos = {
                    VentaId: response.VentaId,
                    EventoId: inst.BoletaSE.EventoId()
                }
                inst.venta = response.VentaId
                AjaxCall("http://localhost:51978/api/DetalleVenta", "post", Datos).done(function () {})
                $("#ventanaEventView").append('<div class="alert alert-success" role="alert"><strong>�Genial!</strong> Has Comprado '+inst.BoletaSE.CantidadVendidos()+' boletas de este evento</div>')
                $("#CloseCMModal").click()
                window.location = "http://localhost:51978/Eventos/FacturaCompra?VentaId=" + inst.venta;
            
            }
        })
    }
    this.ingresarLG = function () {
        if ($('#UserLG').val() != "" && $('#PassLG').val()) {
            AjaxCall("http://localhost:51978/api/Usuario/Login", "post", {
                Nick: $('#UserLG').val(),
                Contrasena: $('#PassLG').val()
            }).done(function (response) {
                if (response.ingreso == true) {
                    $("#ErrorLG").html('<div class="alert alert-success" role="alert"><strong>Listo</strong> <br> Datos Correctos, espere c: </div>');
                    $.cookie("UNA", response.usuario.Nombre.toString());
                    $.cookie("UNI", response.usuario.Nick.toString());
                    $.cookie("UDI", response.usuario.UsuarioId.toString());
                    $.cookie("UROI", response.usuario.RolId.toString());
                    window.location = response.url;

                } else {
                    $("#ErrorLG").html('<div class="alert alert-warning" role="alert"><strong>�Error!</strong> Los datos ingresados son invalidos o incorrectos.</div>');
                }
            });
        } else {
            $("#ErrorLG").html('<div class="alert alert-danger" role="alert"><strong> >:v </strong> Ingrese todos los campos.</div>');
        }
    }

    //Funciones simples
    
    function AjaxCall(uri, method, data) {
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, status, error) {
            main.error(error);
            console.log("jqXHR: " + jqXHR);
            console.log("Status: " + status);
        })
    }
    function getEventos() {
        AjaxCall("http://localhost:51978/api/Evento/Recientes", "get").done(function (response) {
            inst.eventosBase(response)
        })
        AjaxCall("http://localhost:51978/api/Evento/Promocion", "get").done(function (response) {
            inst.eventosPromo(response)
        })
        AjaxCall("http://localhost:51978/api/TipoEvento", "get").done(function (response) {
            console.log(response)
            for (var i = 0; i < response.length; i++) {
                $("#busquedaCAT").append("<option value='" + response[i].TipoEventoId + "'>" + response[i].TipoE + "</option>")
                console.log("<option value='" + response[i].TipoEventoId + "'>" + response[i].TipoE + "</option>")
            }
        })
        
    }
    function buscarEvento(ID) {
        var retornar = null
        var list1 = inst.eventosBase()
        var list2 = inst.eventosPromo()
        for (var i = 0; i < list1.length; i++) {
            if (list1[i].EventoId == ID) {
                retornar = list1[i]
                break;
            }
        }
        for (var i = 0; i < list2.length; i++) {
            if (list2[i].EventoId == ID) {
                retornar = list2[i]
                break;
            }
        }
        return retornar
    }
    getEventos()
}

$(document).ready(function () {
    var vM = new ViewModel();
    ko.applyBindings(vM);
    console.log(vM);
});

//Function for Open Navigator 
function openMenu() {
	$(".nav-page").css("display","block");
	$(".nav-page").css("width","100%");
}

//Function for Close Navigator 
function closeMenu() {
	$(".nav-page").css("display","none");	
}