﻿var VMAD = function () {
    var inst = this;
    //Declaracion de Variables para uso
    inst.eventosRecect = ko.observable();
    inst.usuarios = ko.observable();
    inst.myuser = {
        UsuarioId: ko.observable(),
        Nombre: ko.observable(),
        Nick: ko.observable(),
        Contrasena: ko.observable(),
        Edad: ko.observable(),
        Telefono: ko.observable(),
        Email: ko.observable(),
        NoTarjeta: ko.observable(),
        Rol: ko.observable(),
        RolId: ko.observable()
    }
    inst.USSID  = ko.observable()
    inst.USS = {
        UsuarioId: ko.observable(),
        Nombre: ko.observable(),
        Nick: ko.observable(),
        Contrasena: ko.observable(),
        Edad: ko.observable(),
        Telefono: ko.observable(),
        Email: ko.observable(),
        NoTarjeta: ko.observable(),
        Rol: ko.observable(),
        RolId: ko.observable()
    }
    inst.eventofroUser = ko.observable()
    inst.eventos = ko.observable();
    inst.ventas = ko.observableArray();
    var d = new Date();
    var day = (d.getDate().toString().length == 1) ? "0" + d.getDate() : d.getDate() ;
    var month = (d.getMonth().toString().length == 1) ? "0" + (parseInt(d.getMonth()) +1) : d.getMonth();
    var date = d.getFullYear() + "-" + month  + "-" + day;
    inst.date = ko.observable(date)
    console.log(date)
    inst.tipoPersonals = ko.observable()
    inst.tipoP = {
        TipoPersonalId: ko.observable(0),
        NombreTipoP: ko.observable(),
        Type: ko.observable(1), //1: Tipo Personal, 2: Tipo Evento
    }
    inst.tiposEventos = ko.observable()
    //Declaracion de Funciones para ejecucion
    inst.editarUS = function () {
        var data = {
            UsuarioId: inst.myuser.UsuarioId(),
            Nombre: inst.myuser.Nombre(),
            Nick: inst.myuser.Nick(),
            Contrasena: inst.myuser.Contrasena(),
            Edad: inst.myuser.Edad(),
            Telefono: inst.myuser.Telefono(),
            Email: inst.myuser.Email(),
            NoTarjeta: inst.myuser.NoTarjeta(),
            Rol: inst.myuser.Rol(),
            RolId: inst.myuser.RolId()
        }
        AjaxCall("../api/Usuario/" + data.UsuarioId.toString(), "put", data).done(function () {
            getUsuario();
            $('#edUserMO').click();
            $('#editCR').html('<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Genial c:</strong> Genial se <b>EDITO</b> su usuario c:  <br> Cierra la ventana</div>')

        })
    }
    inst.editarUSER = function () {
        var data = {
            UsuarioId: inst.USS.UsuarioId(),
            Nombre: inst.USS.Nombre(),
            Nick: inst.USS.Nick(),
            Contrasena: inst.USS.Contrasena(),
            Edad: inst.USS.Edad(),
            Telefono: inst.USS.Telefono(),
            Email: inst.USS.Email(),
            NoTarjeta: inst.USS.NoTarjeta(),
            Rol: inst.USS.Rol(),
            RolId: inst.USS.RolId()
        }
        AjaxCall("../api/Usuario/" + data.UsuarioId.toString(), "put", data).done(function () {
            getUsuario();
            $('#edUserMO').click();
            $('#editCR').html('<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Genial c:</strong> Genial se <b>EDITO</b> su usuario c:  <br> Cierra la ventana</div>')

        })
    }
    inst.eliminarUSER = function () {
        AjaxCall("../api/Usuario/" + inst.USS.UsuarioId(), "delete").done(function () {
            getUsuario();
            $('#edUserMO').click();
            $('#editCR').html('<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Genial c:</strong> Genial se <b>EDITO</b> su usuario c:  <br> Cierra la ventana</div>')

        })
    }
    inst.cargarUS = function () {
        getUS()
    }
//Tipos
    inst.cargarTP = function (item) {
        inst.tipoP.TipoPersonalId(item.TipoPersonalId)
        inst.tipoP.NombreTipoP(item.NombreTipoP)
        inst.tipoP.Type(1)
    }
    inst.cargarTE = function (item) {
        inst.tipoP.TipoPersonalId(item.TipoEventoId)
        inst.tipoP.NombreTipoP(item.TipoE)
        inst.tipoP.Type(2)
    }
    inst.AGT = function () {
            if (inst.tipoP.Type() == 1) {
                inst.EDTP()
            } else {
                inst.EDTE()
            }
    }
    inst.ELT = function () {
        if (inst.tipoP.Type() == 1) {
            inst.ELTP()
        } else {
            inst.ELTE()
        }
    }
    inst.AGTP = function () {
        var data = {
            TipoPersonalId: inst.tipoP.TipoPersonalId(),
            NombreTipoP: inst.tipoP.NombreTipoP()
        }
        AjaxCall("../api/TipoPersonal", "post", data).done(function (respones) {
            getTiposP()
            inst.OP = 1
            inst.tipoP.NombreTipoP("")
            $('#closeAGTMO').click()
        })
    }
    inst.AGTE = function () {
        var data = {
            TipoEventoId: inst.tipoP.TipoPersonalId(),
            TipoE: inst.tipoP.NombreTipoP()
        }
        AjaxCall("../api/TipoEvento", "post", data).done(function (respones) {
            getTiposE()
            inst.OP = 1
            inst.tipoP.NombreTipoP("")
            $('#closeAGEMO').click()
        })
    }
    inst.EDTP = function () {
        var data = {
            TipoPersonalId: inst.tipoP.TipoPersonalId(),
            NombreTipoP: inst.tipoP.NombreTipoP()
        }
        AjaxCall("../api/TipoPersonal/"+data.TipoPersonalId, "put", data).done(function (respones) {
            getTiposP()
            inst.OP = 1
            $('#closeEDMO').click()
        })
    }
    inst.EDTE = function () {
        var data = {
            TipoEventoId: inst.tipoP.TipoPersonalId(),
            TipoE: inst.tipoP.NombreTipoP()
        }
        AjaxCall("../api/TipoEvento/" + data.TipoEventoId, "put", data).done(function (respones) {
            getTiposE()
            inst.OP = 1
            $('#closeEDMO').click()
        })
    }
    inst.ELTP = function (formElement) {
        var data = {
            TipoPersonalId: inst.tipoP.TipoPersonalId(),
            NombreTipoP: inst.tipoP.NombreTipoP()
        }
        AjaxCall("../api/TipoPersonal/" + data.TipoPersonalId, "delete").done(function (respones) {
            getTiposP()
            $('#closeELMO').click()
        })
    }
    inst.ELTE = function (formElement) {
        var data = {
            TipoEventoId: inst.tipoP.TipoPersonalId(),
            TipoE: inst.tipoP.NombreTipoP()
        }
        AjaxCall("../api/TipoEvento/" + data.TipoEventoId, "delete").done(function (respones) {
            getTiposE()
            $('#closeELMO').click()
        })
    }
    inst.cancel = function () {
        inst.OP = 1
    }

    inst.eliminaUser = function () {
        AjaxCall("../api/Usuario/" + $.cookie("UDI"), "delete").done(function () {
            $.removeCookie("UNA");
            $.removeCookie("UNI");
            $.removeCookie("UDI");
            $.removeCookie("UROI");
            window.location = "http://localhost:51978";
        })
    }

    inst.cargarUsers = function (item) {
        console.log(item)
        inst.USS.UsuarioId(item.UsuarioId);
        inst.USS.Nombre(item.Nombre);
        inst.USS.Nick(item.Nick);
        inst.USS.Contrasena(item.Contrasena);
        inst.USS.Edad(item.Edad);
        inst.USS.Telefono(item.Telefono);
        inst.USS.Email(item.Email);
        inst.USS.NoTarjeta(item.NoTarjeta);
        inst.USS.Rol(item.Rol);
        inst.USS.RolId(item.RolId);
    }
    inst.cambioTablaUser = function () {
        console.log(inst.USSID().UsuarioId)
        AjaxCall("../api/Evento/Usuario/" + inst.USSID().UsuarioId, "get").done(function (response) {
            if (response != null) {
                inst.eventofroUser(response);
            }
        })
    }
    //Declaracion de Funciones Simples
    function getUS() {
        AjaxCall("../api/Usuario/" + $.cookie("UDI"), "get").done(function (response) {
            inst.USS.UsuarioId(response.UsuarioId);
            inst.USS.Nombre(response.Nombre);
            inst.USS.Nick(response.Nick);
            inst.USS.Contrasena(response.Contrasena);
            inst.USS.Edad(response.Edad);
            inst.USS.Telefono(response.Telefono);
            inst.USS.Email(response.Email);
            inst.USS.NoTarjeta(response.NoTarjeta);
            inst.USS.Rol(response.Rol);
            inst.USS.RolId(response.RolId);
        })
        return respo
    }

    function AjaxCall(uri, method, data) {
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, status, error) {
            inst.error = error;
            console.log("jqXHR: " + jqXHR);
            console.log("Status: " + status);
            console.log("Error: " + error);
        })
    }
    function getEventos() {
        AjaxCall("../api/Evento/Usuario/" + $.cookie("UDI"), "get").done(function (response) {
            if (response != null) {
                inst.eventos(response);
            }
        })
    }
    function getUsuario() {
        AjaxCall("../api/Usuario", "get").done(function (response) {
            if (response != null) {
                inst.usuarios(response);
            }
        })
    }
    function getVentasDay() {
        AjaxCall("../api/Venta/Fecha/" + date, "get").done(function (response) {
            if (response != null) {
                console.log(response)
                inst.ventas(response);
            }
        })
    }
    function getVentasREC() {
        AjaxCall("../api/Evento/Recientes", "get").done(function (response) {
            if (response != null) {
                inst.eventosRecect(response);
            }
        })
    }
    function getTiposP() {
        AjaxCall("../api/TipoPersonal", "get").done(function (response) {
            if (response != null) {
                inst.tipoPersonals(response);
            }
        })
    }
    function getTiposE() {
        AjaxCall("../api/TipoEvento", "get").done(function (response) {
            if (response != null) {
                inst.tiposEventos(response);
            }
        })
    }

    function CargarOpciones() {
        $("#linksToHeader").html("");
        $("#linksToHeader").append('<a href="#" data-bind="click: $root.cargarUS" data-toggle="modal" data-target="#edUserMO"><h3 class="text-center" style="color: #fff">Editar mi Perfil</h3></a>');
        $("#linksToHeader").append('<a href="Types" data-bind="click: $root.Graficas" ><h3 class="text-center" style="color: #fff">Administrar Tipos</h3></a>');
        $("#linksToHeader").append('<a href="Graficas" data-bind="click: $root.Graficas" ><h3 class="text-center" style="color: #fff">Generar Reporte y Grafica</h3></a>');
        $("#linksToHeader").append('<a href="#" onclick="eliminaUser()" ><h3 class="text-center" style="color: #fff">Eliminar Mi Cuenta</h3></a>');
        $("#linksToHeader").append('<a href="#" onclick="logOut()" ><h3 class="text-center" style="color: #fff">Cerrar Session</h3></a>');
    }
    CargarOpciones()
    getUsuario()
    getEventos()
    getVentasREC()
    getVentasDay()
    getTiposE()
    getTiposP()
}

$(document).ready(function () {
    var vv = new VMAD();
    ko.applyBindings(vv);

})

function clearInputs() {
    $("input").val("");
    $("textarea").val("");
}

//Function for Open Navigator 
function openMenu() {
    $(".nav-page").css("display", "block");
    $(".nav-page").css("width", "100%");
}
//Function for Close Navigator 
function closeMenu() {
    $(".nav-page").css("display", "none");
}


function eliminaUser(){
    AjaxCall("../api/Usuario/"+$.cookie("UDI"), "delete").done(function() {
        $.removeCookie('UNA', { path: '/' });
        $.removeCookie('UNI', { path: '/' });
        $.removeCookie('UDI', { path: '/' });
        $.removeCookie('UROI', { path: '/' });
        window.location = "http://localhost:51978";
    })
}
function logOut() {
    console.log("Filñ uwu");
    $.removeCookie('UNA', { path: '/' });
    $.removeCookie('UNI', { path: '/' });
    $.removeCookie('UDI', { path: '/' });
    $.removeCookie('UROI', { path: '/' });
    window.location = "http://localhost:51978";
}