﻿$(document).ready(function () {
    dataooo()
    CargarOpciones()
})

function CargaDatos(data) {
    var ctx = document.getElementById("graficaData");
    console.log(data);
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: data.NombreEvento,
            datasets: [{
                label: '# of Votes',
                data: data.cuenta,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            response:true
        }
    });
}

function dataooo() {
    AjaxCall("http://localhost:51978/api/Venta/MasVentas", "get").done(function (response) {
        var data = { NombreEvento: [], cueta: [] }
        data = response;
        CargaDatos(data)
        for (var i = 0 ; i< data.NombreEvento.length; i++) {
            $("#TablaReporte").append("<tr><td>" + (i + 1) + "</td><td>" + data.NombreEvento[i] + "</td><td>" + data.cuenta[i] + "</td></tr>")
        }
    })
}

function AjaxCall(uri, method, data) {
    return $.ajax({
        url: uri,
        type: method,
        dataType: 'json',
        contentType: 'application/json',
        data: data ? JSON.stringify(data) : null
    }).fail(function (jqXHR, status, error) {
        inst.error = error;
        console.log("jqXHR: " + jqXHR);
        console.log("Status: " + status);
        console.log("Error: " + error);
    })
}

//Function for Open Navigator 
function openMenu() {
    $(".nav-page").css("display", "block");
    $(".nav-page").css("width", "100%");
}
function logOut() {
    console.log("Fin uwu");
    $.removeCookie("UNA");
    $.removeCookie("UNI");
    $.removeCookie("UDI");
    $.removeCookie("UROI");
    window.location = "http://localhost:51978";
}
//Function for Close Navigator 
function closeMenu() {
    $(".nav-page").css("display", "none");
}
function CargarOpciones() {
    $("#linksToHeader").html("");
    $("#linksToHeader").append('<a href="#" data-bind="click: $root.cargarUS" data-toggle="modal" data-target="#edUserMO"><h3 class="text-center" style="color: #fff">Editar mi Perfil</h3></a>');
    $("#linksToHeader").append('<a href="Types" data-bind="click: $root.Graficas" ><h3 class="text-center" style="color: #fff">Administrar Tipos</h3></a>');
    $("#linksToHeader").append('<a href="#" data-bind="click: $root.Graficas" ><h3 class="text-center" style="color: #fff">Generar Reporte y Grafica</h3></a>');
    $("#linksToHeader").append('<a href="#" onclick="logOut()" ><h3 class="text-center" style="color: #fff">Cerrar Session</h3></a>');
}