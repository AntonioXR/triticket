﻿var VMUS =function(){
// --------------------------- Creacion de variables para formularios y listados ---------------------------
var inst = this;
inst.usuarios = ko.observable();
inst.tipoEventos = ko.observable();    
inst.eventos = ko.observable();
inst.AGE = {
    EventoId: ko.observable(),
    NombreEvento: ko.observable(),
    Existencias: ko.observable(0),
    FechaEvento: ko.observable(),
    Hora: ko.observable(),
    Precio: ko.observable(0.0),
    TipoEventoId:0,
    TipoEvento: ko.observable(),
    Lugar: ko.observable(),
    Descripcion: ko.observable(),
    Promociones: ko.observable(0),
    UsuarioId:0,
    Usuario: ko.observable()
}
inst.USS = {
    UsuarioId: ko.observable(),
    Nombre: ko.observable(),
    Nick: ko.observable(),
    Contrasena: ko.observable(),
    Edad: ko.observable(),
    Telefono: ko.observable(),
    Email: ko.observable(),
    NoTarjeta: ko.observable(),
    Rol: ko.observable(),
    RolId: ko.observable()
}
inst.PEvent = {
    PersonalEventoId: ko.observable(),
    NombrePersona: ko.observable(),
    Evento: inst.AGE,
    PrecioPorHora: ko.observable(),
    TipoPersonal: ko.observable()
}
inst.PEventos = ko.observable();
inst.tipoP = ko.observable()
    // --------------------------- Funciones para Formularios y Ejecuciones ---------------------------
inst.CargarPersonal = function (item) {
    inst.eventSRC = (item)
    getPersonal(item.EventoId)
}
    
inst.agregarPersonal = function (fromElement) {
    var data= {
        PersonalEventoId: inst.PEvent.PersonalEventoId(),
        NombrePersona: inst.PEvent.NombrePersona(),
        EventoId: inst.eventSRC.EventoId,
        PrecioPorHora: inst.PEvent.PrecioPorHora(),
        TipoPersonalId: inst.PEvent.TipoPersonal().TipoPersonalId
    }
    AjaxCall("../api/PersonalEvento", "post", data).done(function () {
        getPersonal(inst.eventSRC.EventoId);
    })
}
inst.agregarEve = function (formElement) {
    var dateti = inst.AGE.FechaEvento() + " " + inst.AGE.Hora();
    var data = {
    NombreEvento: inst.AGE.NombreEvento(),
    Existencias: inst.AGE.Existencias(),
    FechaEvento: dateti,
    Precio: inst.AGE.Precio(),
    TipoEventoId: inst.AGE.TipoEvento().TipoEventoId,
    TipoEvento: inst.AGE.TipoEvento(),
    Lugar: inst.AGE.Lugar(),
    Descripcion: inst.AGE.Descripcion(),
    Promociones: inst.AGE.Promociones(),
    UsuarioId: parseInt($.cookie("UDI")),
    Usuario: getUser(parseInt($.cookie("UDI")))
    };
    AjaxCall("../api/Evento", "post",data).done(function(response) {
        getEventos();
        inst.AGE = {
            EventoId: ko.observable(),
            NombreEvento: ko.observable(),
            Existencias: ko.observable(0),
            FechaEvento: ko.observable(),
            Hora: ko.observable(),
            Precio: ko.observable(0.0),
            TipoEventoId: 0,
            TipoEvento: ko.observable(),
            Lugar: ko.observable(),
            Descripcion: ko.observable(),
            Promociones: ko.observable(0),
            UsuarioId: 0,
            Usuario: ko.observable()
        }
        $('#CloseAGEMO').click();
        $('#editCR').html('<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Genial c:</strong> Genial se <b>AGREGO</b> el evento c:  <br> Cierra la ventana</div>')

    });
}

inst.cargarUS = function () {
    getMyProfial()
}

inst.editarUS = function () {
    var data = {
        UsuarioId:inst.USS.UsuarioId(),
        Nombre:inst.USS.Nombre(),
        Nick:inst.USS.Nick(),
        Contrasena:inst.USS.Contrasena(),
        Edad:inst.USS.Edad(),
        Telefono:inst.USS.Telefono(),
        Email:inst.USS.Email(),
        NoTarjeta:inst.USS.NoTarjeta(),
        Rol:inst.USS.Rol(),
        RolId:inst.USS.RolId()
    }
    AjaxCall("../api/Usuario/" + data.UsuarioId.toString(), "put", data).done(function () {
        getMyProfial()
        $('#edUserMO').click();
        $('#editCR').html('<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Genial c:</strong> Genial se <b>EDITO</b> su usuario c:  <br> Cierra la ventana</div>')

    })
}

inst.editarEve = function () {
    var dateti = inst.AGE.FechaEvento() + " " + inst.AGE.Hora();
    var data = {
        EventoId: inst.AGE.EventoId(),
        NombreEvento: inst.AGE.NombreEvento(),
        Existencias: inst.AGE.Existencias(),
        FechaEvento: dateti,
        Precio: inst.AGE.Precio(),
        TipoEventoId:inst.AGE.TipoEventoId,
        TipoEvento: inst.AGE.TipoEvento(),
        Lugar: inst.AGE.Lugar(),
        Descripcion: inst.AGE.Descripcion(),
        Promociones: inst.AGE.Promociones(),
        UsuarioId:inst.AGE.Usuario.UsuarioId,
        Usuario: inst.AGE.Usuario()
    };
    AjaxCall("../api/Evento/" + inst.AGE.EventoId().toString(), "put", data).done(function () {
        getEventos();
        inst.AGE = {
            EventoId: ko.observable(),
            NombreEvento: ko.observable(),
            Existencias: ko.observable(0),
            FechaEvento: ko.observable(),
            Precio: ko.observable(0.0),
            TipoEventoId: 0,
            TipoEvento: ko.observable(),
            Lugar: ko.observable(),
            Descripcion: ko.observable(),
            Promociones: ko.observable(0),
            UsuarioId: 0,
            Usuario: ko.observable()
        }
        $('#CloseEDIMO').click();
        $('#editCR').html('<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Genial c:</strong> Genial se <b>EDITO</b> el evento c:  <br> Cierra la ventana</div>')
    });
}

inst.cargarEve = function (item) {
    var date = getDate(0, item.FechaEvento);
    var time = getDate(date.length + 1, item.FechaEvento);
    inst.AGE.EventoId(item.EventoId)
    inst.AGE.NombreEvento(item.NombreEvento)
    inst.AGE.Existencias(item.Existencias)
    inst.AGE.FechaEvento(date)
    inst.AGE.Hora(time)
    inst.AGE.Precio(item.Precio)
    inst.AGE.TipoEventoId = item.TipoEventoId
    inst.AGE.TipoEvento(item.TipoEvento)
    inst.AGE.Lugar(item.Lugar)
    inst.AGE.Descripcion(item.Descripcion)
    inst.AGE.Promociones(item.Promociones)
    inst.AGE.Usuario.UsuarioId =item.UsuarioId
    inst.AGE.Usuario(item.Usuario)
}

inst.eliminarEve = function (formElement) {
    AjaxCall("../api/Evento/" + inst.AGE.EventoId().toString() , "delete").done(function () {
        getEventos();
    });
    $('#CloseEDIMO').click();
    $('#editCR').html('<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Genial c:</strong> Genial se <b>ELIMINO</b> el evento c:  <br> Cierra la ventana</div>')

}

// --------------------------- Funciones simples ---------------------------
function AjaxCall(uri, method, data) {
    return $.ajax({
        url: uri,
        type: method,
        dataType: 'json',
        contentType: 'application/json',
        data: data ? JSON.stringify(data) : null
    }).fail(function (jqXHR, status, error) {
        inst.error= error;
        console.log("jqXHR: " + jqXHR);
        console.log("Status: " + status);
        console.log("Error: " + error);
    })
}

function getUsuario() {
    AjaxCall("../api/Usuario", "get").done(function (response) {
        if (response != null) {
            inst.usuarios(response);
        }
    });
}
function getTipoEvento() {
    AjaxCall("../api/TipoEvento", "get").done(function (response) {
        if (response != null) {
            inst.tipoEventos(response);
        }
    });
}
function getEventos() {
    AjaxCall("../api/Evento/Usuario/"+$.cookie("UDI"), "get").done(function (response) {
        if (response != null) {
            inst.eventos(response);
        }
    });
}
function getPersonalType() {
    AjaxCall("../api/TipoPersonal/", "get").done(function (response) {
        if (response != null) {
            console.log(response)
            inst.tipoP(response);
        }
    });
}
function getPersonal(ID) {
    console.log(ID)
    AjaxCall("../api/PersonalEvento/Evento/"+ID, "get").done(function (response) {
        if (response != null) {
            console.log(response)
            inst.PEventos(response);
        }
    });
}
function getMyProfial() {
    AjaxCall("../api/Usuario/"+$.cookie("UDI"), "get").done(function (response) {
        if (response != null) {
         inst.USS.UsuarioId(response.UsuarioId);
         inst.USS.Nombre(response.Nombre);
         inst.USS.Nick(response.Nick);
         inst.USS.Contrasena(response.Contrasena);
         inst.USS.Edad(response.Edad);
         inst.USS.Telefono(response.Telefono);
         inst.USS.Email(response.Email);
         inst.USS.NoTarjeta(response.NoTarjeta);
         inst.USS.Rol(response.Rol);
         inst.USS.RolId(response.RolId);
        }
    });
}

function getUser(ID) {
    var list = inst.usuarios();
    for(var i = 0; i < list.length; i++) {
        if (list[i].UsuarioId == ID) {
            return list[i];
            break;
        }
    }
    return null;
}
function getDate(inicio, stri) {
    var ini = inicio;
    var i = inicio +1;
    while (true) {
        if (stri.length == i) {
            return stri.substring(ini, i)
            break;
        }
        if (stri.substring(inicio, i) == "T") {
            return stri.substring(ini , i - 1);
        }
        i++;
        inicio++;
    }

}

function CargarOpciones() {
    $("#linksToHeader").html("");
    $("#linksToHeader").append('<a href="#" data-bind="click: $root.cargarUS" data-toggle="modal" data-target="#edUserMO"><h3 class="text-center" style="color: #fff">Editar mi Perfil</h3></a>');
    $("#linksToHeader").append('<a href="#" onclick="eliminaUser()" ><h3 class="text-center" style="color: #fff">Eliminar Mi Cuenta</h3></a>');
    $("#linksToHeader").append('<a href="#" onclick="logOut()" ><h3 class="text-center" style="color: #fff">Cerrar Session</h3></a>');
}

getUsuario();
getEventos();
getTipoEvento();
CargarOpciones();
getPersonalType()
}

$(document).ready(function () {
    var vv= new VMUS();
    ko.applyBindings(vv);

})

function clearInputs(){
    $("input").val("");
    $("textarea").val("");
}

//Function for Open Navigator 
function openMenu() {
    $(".nav-page").css("display", "block");
    $(".nav-page").css("width", "100%");
}

//Function for Close Navigator 
function closeMenu() {
    $(".nav-page").css("display", "none");
}


function eliminaUser(){
    AjaxCall("../api/Usuario/"+$.cookie("UDI"), "delete").done(function() {
        $.removeCookie('UNA', { path: '/' });
        $.removeCookie('UNI', { path: '/' });
        $.removeCookie('UDI', { path: '/' });
        $.removeCookie('UROI', { path: '/' });
        window.location = "http://localhost:51978";
    })
}
function logOut() {
    console.log("Filñ uwu");
    $.removeCookie('UNA', { path: '/' });
    $.removeCookie('UNI', { path: '/' });
    $.removeCookie('UDI', { path: '/' });
    $.removeCookie('UROI', { path: '/' });
    window.location = "http://localhost:51978";
}