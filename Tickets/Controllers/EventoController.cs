﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class EventoController : ApiController
    {
        private TicketsContext db = new TicketsContext();

        // GET: api/Evento
        public IQueryable<Evento> GetEvento()
        {
            return db.Evento;
        }

        // GET: api/Evento/5
        [ResponseType(typeof(Evento))]
        public async Task<IHttpActionResult> GetEvento(long id)
        {
            Evento evento = await db.Evento.FindAsync(id);
            if (evento == null)
            {
                return NotFound();
            }

            return Ok(evento);
        }

        //GET: api/Evento/Tipo/{TipoEventoId}
        [HttpGet, Route("api/Evento/Tipo/{TipoEventoId}")]
        public IQueryable<Evento> getType(int TipoEventoId)
        {
            return (from us in db.Evento where us.TipoEventoId== TipoEventoId || us.TipoEvento.TipoEventoId == TipoEventoId select us);
        }

        [HttpGet, Route("api/Evento/Calendar/{UsuarioId}")]
        public List<object> getCalendar(int UsuarioId)
        {
            List<object> eventos = new List<object>();
            foreach(Evento ev in (from eve in db.Evento where eve.UsuarioId == UsuarioId select eve))
            {
                eventos.Add(new { title= ev.NombreEvento, date= ev.FechaEvento.Date, and="Lugar: "+ev.Lugar+" Precio: "+ev.Precio});
            }
            return eventos;
        }

        [HttpGet, Route("api/Evento/Calendar")]
        public List<object> getCalendar()
        {
            List<object> eventos = new List<object>();
            foreach (Evento ev in (from eve in db.Evento select eve))
            {
                eventos.Add(new { title = ev.NombreEvento, date = ev.FechaEvento.Date.ToString().Substring(0,10).Replace('/','-'), location = "Lugar: " + ev.Lugar + " Precio: " + ev.Precio });
            }
            return eventos;
        }

        //GET: api/Evento/Tipo/{TipoEventoId}
        [HttpGet, Route("api/Evento/Recientes")]
        public IQueryable<Evento> getRecientes()
        {
            return (from us in db.Evento select us).OrderByDescending(t => t.FechaEvento).Take(6);
        }

        //GET: api/Evento/Tipo/{Date}
        [HttpGet, Route("api/Evento/Fecha/{Date}")]
        public IQueryable<Evento> getDate(String Date)
        {
            
            return (from us in db.Evento where us.FechaEvento == DateTime.Parse(Date) select us);
        }

        //GET: api/Evento/Usuario/{UsuarioId}
        [HttpGet, Route("api/Evento/Usuario/{UsuarioId}")]
        public IQueryable<Evento> getUsuario(int UsuarioId)
        {

            return (from us in db.Evento where us.Usuario.UsuarioId ==  UsuarioId || us.UsuarioId == UsuarioId select us);
        }

        //GET: api/Evento/Tipo/{Date}
        [HttpGet, Route("api/Evento/Promocion")]
        public IQueryable<Evento> getPromo()
        {
            return (from us in db.Evento where us.Promociones != 0 select us);
        }

        // PUT: api/Evento/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEvento(long id, Evento evento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != evento.EventoId)
            {
                return BadRequest();
            }
            evento.TipoEvento = (from te in db.TipoEvento where te.TipoEventoId == evento.TipoEventoId select te).First();
            evento.Usuario = (from us in db.Usuario where evento.UsuarioId == us.UsuarioId select us).First();
            db.Entry(evento).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Evento
        [ResponseType(typeof(Evento))]
        public async Task<IHttpActionResult> PostEvento(Evento evento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            evento.TipoEvento = (from te in db.TipoEvento where te.TipoEventoId == evento.TipoEventoId select te).First();
            evento.Usuario = (from us in db.Usuario where evento.UsuarioId == us.UsuarioId select us).First();
            
            db.Evento.Add(evento);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = evento.EventoId }, evento);
        }

        // DELETE: api/Evento/5
        [ResponseType(typeof(Evento))]
        public async Task<IHttpActionResult> DeleteEvento(long id)
        {
            Evento evento = await db.Evento.FindAsync(id);
            if (evento == null)
            {
                return NotFound();
            }

            db.Evento.Remove(evento);
            await db.SaveChangesAsync();

            return Ok(evento);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EventoExists(long id)
        {
            return db.Evento.Count(e => e.EventoId == id) > 0;
        }
    }
}