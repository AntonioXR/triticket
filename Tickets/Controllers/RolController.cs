﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class RolController : ApiController
    {
        private TicketsContext db = new TicketsContext();

        // GET: api/Rol
        public IQueryable<Rol> GetRol()
        {
            return db.Rol;
        }

        // GET: api/Rol/5
        [ResponseType(typeof(Rol))]
        public async Task<IHttpActionResult> GetRol(long id)
        {
            Rol rol = await db.Rol.FindAsync(id);
            if (rol == null)
            {
                return NotFound();
            }

            return Ok(rol);
        }

        // PUT: api/Rol/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRol(long id, Rol rol)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rol.RolId)
            {
                return BadRequest();
            }

            db.Entry(rol).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RolExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Rol
        [ResponseType(typeof(Rol))]
        public async Task<IHttpActionResult> PostRol(Rol rol)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Rol.Add(rol);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = rol.RolId }, rol);
        }

        // DELETE: api/Rol/5
        [ResponseType(typeof(Rol))]
        public async Task<IHttpActionResult> DeleteRol(long id)
        {
            Rol rol = await db.Rol.FindAsync(id);
            if (rol == null)
            {
                return NotFound();
            }

            db.Rol.Remove(rol);
            await db.SaveChangesAsync();

            return Ok(rol);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RolExists(long id)
        {
            return db.Rol.Count(e => e.RolId == id) > 0;
        }
    }
}