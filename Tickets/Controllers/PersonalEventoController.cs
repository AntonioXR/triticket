﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class PersonalEventoController : ApiController
    {
        private TicketsContext db = new TicketsContext();

        // GET: api/PersonalEvento
        public IQueryable<PersonalEvento> GetPersonalEvento()
        {
            return db.PersonalEvento;
        }

        // GET: api/PersonalEvento/5
        [ResponseType(typeof(PersonalEvento))]
        public async Task<IHttpActionResult> GetPersonalEvento(long id)
        {
            PersonalEvento personalEvento = await db.PersonalEvento.FindAsync(id);
            if (personalEvento == null)
            {
                return NotFound();
            }

            return Ok(personalEvento);
        }

        // PUT: api/PersonalEvento/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPersonalEvento(long id, PersonalEvento personalEvento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != personalEvento.PersonalEventoId)
            {
                return BadRequest();
            }

            db.Entry(personalEvento).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonalEventoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PersonalEvento
        [ResponseType(typeof(PersonalEvento))]
        public async Task<IHttpActionResult> PostPersonalEvento(PersonalEvento personalEvento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PersonalEvento.Add(personalEvento);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = personalEvento.PersonalEventoId }, personalEvento);
        }

        // DELETE: api/PersonalEvento/5
        [ResponseType(typeof(PersonalEvento))]
        public async Task<IHttpActionResult> DeletePersonalEvento(long id)
        {
            PersonalEvento personalEvento = await db.PersonalEvento.FindAsync(id);
            if (personalEvento == null)
            {
                return NotFound();
            }

            db.PersonalEvento.Remove(personalEvento);
            await db.SaveChangesAsync();

            return Ok(personalEvento);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PersonalEventoExists(long id)
        {
            return db.PersonalEvento.Count(e => e.PersonalEventoId == id) > 0;
        }

        [Route("api/PersonalEvento/Evento/{EventoId}"), HttpGet]
        public IQueryable<PersonalEvento> getForEvent(int EventoId) {
            return (from prE in db.PersonalEvento where prE.Evento.EventoId == EventoId select prE).Include(item => item.Evento).Include(item => item.TipoPersonal);
        }
        
    }
}