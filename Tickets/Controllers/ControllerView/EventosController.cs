﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Tickets.Models;
namespace Tickets.Controllers.ControllerView
{
    public class EventosController : Controller
    {
        // GET: Eventos
        private TicketsContext db = new TicketsContext();
        
        public ActionResult Index(String Busqueda)
        {
            ViewData["BusID"] = "";
            ViewData["BusTX"] = "";
            ViewData["IDC"] = "1";
            ViewData["tiposE"] = new List<TipoEvento>((from te in db.TipoEvento select te));
            List<Evento> eventos = new List<Evento>();
            try
            {
                
                int ID = Convert.ToInt32(Busqueda);
                eventos = new List<Evento>((from ev in db.Evento where ev.TipoEventoId == ID && ev.Existencias > 0 select ev));
                ViewData["valueB"] = (from te in db.TipoEvento where te.TipoEventoId == ID select te.TipoE).First();
                ViewData["BusID"] = "active";
                ViewData["IDC"] = ID;
                
            }
            catch(Exception ex)
            {
                eventos = new List<Evento>((from ev in db.Evento where ev.NombreEvento.Contains(Busqueda) && ev.Existencias > 0 select ev));
                ViewData["valueB"] = Busqueda;
                ViewData["BusTX"] = "active";
            }
            return View(eventos);
        }
        public ActionResult EventoView(int ID)
        {
            if(ID != 0)
            {
                Evento ev = ((from even in db.Evento where even.EventoId == ID select even).Count() == 0) ? new Evento() { EventoId = 0, NombreEvento="Evento no Encontrado", Descripcion="Hemos tenido un error al buscar este Evento, revise el enlace, este evento a sido eliminado o no se tienen en el sistema"} :
                    (from even in db.Evento where even.EventoId == ID select even).First();
                ev.TipoEvento = (from te in db.TipoEvento where ev.TipoEventoId == te.TipoEventoId select te).First();
                return View(ev);
            }
            return View(new Evento() {TipoEvento = new TipoEvento() ,EventoId = 0, NombreEvento = "ID NO VALIDO", Descripcion = "Hemos tenido un error al buscar este Evento, revise el enlace y vuelva a ingresar" } );
        }
        public ActionResult FacturaCompra(int VentaId) {
            Response.Clear();
            try
            {
                Venta data = (from ve in db.Venta where ve.VentaId == VentaId select ve).First();
                int cant = (from de in db.DetalleVenta where de.VentaId == VentaId select de).Count();
                DetalleVenta deta= (from date in db.DetalleVenta where date.VentaId == VentaId select date).First();
                Evento evn = (from e in db.Evento where e.EventoId == deta.EventoId select e).First();

                Document document = new Document();
                FileStream dile = new FileStream(Server.MapPath("~/App_Data/Factura.pdf"), FileMode.Create);
                PdfWriter.GetInstance(document, dile);
                document.Open();
                Paragraph titulo = new Paragraph("Triticket\nTickets & Mas Guatemala S.A.", new Font(Font.FontFamily.COURIER, 20, 1));
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);

                Paragraph fecha = new Paragraph(data.Fecha.ToString()+" | Factura No. "+data.VentaId, new Font(Font.FontFamily.HELVETICA, 14));
                fecha.Alignment = Element.ALIGN_CENTER;
                document.Add(fecha);

                Paragraph Nombre = new Paragraph("Nombre: "+data.NombreComprador, new Font(Font.FontFamily.HELVETICA, 14));
                Nombre.Alignment = Element.ALIGN_CENTER;
                document.Add(Nombre);

                Paragraph Nit = new Paragraph("NIT: "+data.NIT, new Font(Font.FontFamily.HELVETICA, 14));
                Nit.Alignment = Element.ALIGN_CENTER;
                document.Add(Nit);

                Paragraph Eve = new Paragraph("Venta de "+cant+" Entradas", new Font(Font.FontFamily.HELVETICA, 14));
                Eve.Alignment = Element.ALIGN_CENTER;
                document.Add(Eve);

                Paragraph NoEVE = new Paragraph("Evento: "+evn.NombreEvento, new Font(Font.FontFamily.HELVETICA, 14));
                NoEVE.Alignment = Element.ALIGN_CENTER;
                document.Add(NoEVE);

                Paragraph total = new Paragraph("Total: Q" + data.TotalVenta+".00", new Font(Font.FontFamily.HELVETICA, 14));
                total.Alignment = Element.ALIGN_CENTER;
                document.Add(total);

                document.Close();
                dile.Close();
                dile = null;
                Response.AddHeader("Content-Disposition", "attachment; filename=Factura.pdf");
                Response.ContentType = "application/pdf";
                Response.WriteFile(Server.MapPath("~/App_Data/Factura.pdf"));
                Response.End();
                return View();
            }
            catch (DocumentException ex)
            {
                return View();
            }
        }


        public ActionResult GenerarReporte()
        {
            Response.Clear();
            try
            {
                IQueryable<Venta> ven = (from vent in db.Venta where vent.Fecha.Month == DateTime.Now.Month select vent);
                Document document = new Document();
                FileStream dile = new FileStream(Server.MapPath("~/App_Data/Reporte.pdf"), FileMode.Create);
                PdfWriter.GetInstance(document, dile);
                document.Open();
                Paragraph titulo = new Paragraph("Triticket\nTickets & Mas Guatemala S.A.", new Font(Font.FontFamily.COURIER, 20, 1));
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);

                Paragraph fecha = new Paragraph("Reporte De Venta | Genera el Dia: "+DateTime.Now.ToString(), new Font(Font.FontFamily.HELVETICA, 14));
                fecha.Alignment = Element.ALIGN_CENTER;
                document.Add(fecha);

                foreach (Venta vent in ven) {
                    Paragraph noVEnta = new Paragraph("--------------------Venta No.: " + vent.VentaId+ " Fecha: "+vent.Fecha.ToString()+"--------------------", new Font(Font.FontFamily.HELVETICA, 14));
                    noVEnta.Alignment = Element.ALIGN_CENTER;
                    document.Add(noVEnta);
                    if ((from ddeco in db.DetalleVenta where ddeco.VentaId == vent.VentaId select ddeco).Count() != 0) {
                        Paragraph Detalle = new Paragraph("Evento: " + (from dv in db.DetalleVenta where dv.VentaId == vent.VentaId select dv.Evento.NombreEvento).First() + " No.Vendidos: " + (from dv in db.DetalleVenta where dv.VentaId == vent.VentaId select dv).Count(), new Font(Font.FontFamily.HELVETICA, 14));
                        Detalle.Alignment = Element.ALIGN_CENTER;
                        document.Add(Detalle);
                    }
                }

                Paragraph ul = new Paragraph("--------------------------Ultima linea--------------------------", new Font(Font.FontFamily.HELVETICA, 14));
                ul.Alignment = Element.ALIGN_CENTER;
                document.Add(ul);
                document.Close();
                dile.Close();
                dile = null;
                Response.AddHeader("Content-Disposition", "attachment; filename=Reporte.pdf");
                Response.ContentType = "application/pdf";
                Response.WriteFile(Server.MapPath("~/App_Data/Reporte.pdf"));
                Response.End();
                return View();
            }
            catch (DocumentException ex)
            {
                return View();
            }
        }
    }
}