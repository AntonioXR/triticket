﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tickets.Models;

namespace Tickets.Controllers.ControllerView
{
    public class UsuariosController : Controller
    {
        private TicketsContext db = new TicketsContext();

        // GET: Usuarios
        public ActionResult Index()
        {
            var usuario = db.Usuario.Include(u => u.Rol);
            return View(usuario.ToList());
        }

        public ActionResult IndexAdmin()
        {
            var usuario = db.Usuario.Include(u => u.Rol);
            return View(usuario.ToList());
        }

        public ActionResult Types()
        {
            return View();
        }

        public ActionResult Graficas()
        {
            return View();
        }
        public ActionResult Calendarios()
        {
            return View();
        }
    }
}
