﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class TipoPersonalController : ApiController
    {
        private TicketsContext db = new TicketsContext();

        // GET: api/TipoPersonal
        public IQueryable<TipoPersonal> GetTipoPersonal()
        {
            return db.TipoPersonal;
        }

        // GET: api/TipoPersonal/5
        [ResponseType(typeof(TipoPersonal))]
        public async Task<IHttpActionResult> GetTipoPersonal(long id)
        {
            TipoPersonal tipoPersonal = await db.TipoPersonal.FindAsync(id);
            if (tipoPersonal == null)
            {
                return NotFound();
            }

            return Ok(tipoPersonal);
        }

        // PUT: api/TipoPersonal/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTipoPersonal(long id, TipoPersonal tipoPersonal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoPersonal.TipoPersonalId)
            {
                return BadRequest();
            }

            db.Entry(tipoPersonal).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoPersonalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipoPersonal
        [ResponseType(typeof(TipoPersonal))]
        public async Task<IHttpActionResult> PostTipoPersonal(TipoPersonal tipoPersonal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TipoPersonal.Add(tipoPersonal);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = tipoPersonal.TipoPersonalId }, tipoPersonal);
        }

        // DELETE: api/TipoPersonal/5
        [ResponseType(typeof(TipoPersonal))]
        public async Task<IHttpActionResult> DeleteTipoPersonal(long id)
        {
            TipoPersonal tipoPersonal = await db.TipoPersonal.FindAsync(id);
            if (tipoPersonal == null)
            {
                return NotFound();
            }

            db.TipoPersonal.Remove(tipoPersonal);
            await db.SaveChangesAsync();

            return Ok(tipoPersonal);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TipoPersonalExists(long id)
        {
            return db.TipoPersonal.Count(e => e.TipoPersonalId == id) > 0;
        }
    }
}