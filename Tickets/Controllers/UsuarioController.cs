﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class UsuarioController : ApiController
    {
        private TicketsContext db = new TicketsContext();

        // GET: api/Usuario
        public IQueryable<Usuario> GetUsuario()
        {
            return db.Usuario;
        }

        // GET: api/Usuario/5
        [ResponseType(typeof(Usuario))]
        public async Task<IHttpActionResult> GetUsuario(long id)
        {
            Usuario usuario = await db.Usuario.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }

            return Ok(usuario);
        }

        //POST: api/Usuario/Login
        [Route("api/Usuario/Login"), HttpPost]
        public object Login(Usuario user)
        {
           int i = (from us in db.Usuario where us.Nick.Equals(user.Nick) && us.Contrasena.Equals(user.Contrasena) select us).Count();
            if(i == 0)
            {
                return new { ingreso = false };
            }
            Usuario userLOG = (from us in db.Usuario where us.Nick.Equals(user.Nick) && us.Contrasena.Equals(user.Contrasena) select us).First();
            String URI = (userLOG.RolId == 1)? "/Usuarios/IndexAdmin" : "/Usuarios/Index";
            return new { ingreso = true, url=URI, usuario = userLOG };
        }

        //GET: api/Usuario/Rol/{RolId}
        [HttpGet, Route("api/Usuario/Rol/{RolId}")]
        public IQueryable<Usuario> getUsersRol(int RolId)
        {
            return (from use in db.Usuario where use.Rol.RolId == RolId || use.RolId == RolId select use);
        }

        // PUT: api/Usuario/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUsuario(long id, Usuario usuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != usuario.UsuarioId)
            {
                return BadRequest();
            }

            db.Entry(usuario).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Usuario
        [ResponseType(typeof(Usuario))]
        public async Task<IHttpActionResult> PostUsuario(Usuario usuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Usuario.Add(usuario);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = usuario.UsuarioId }, usuario);
        }

        // DELETE: api/Usuario/5
        [ResponseType(typeof(Usuario))]
        public async Task<IHttpActionResult> DeleteUsuario(long id)
        {
            Usuario usuario = await db.Usuario.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }

            db.Usuario.Remove(usuario);
            await db.SaveChangesAsync();

            return Ok(usuario);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UsuarioExists(long id)
        {
            return db.Usuario.Count(e => e.UsuarioId == id) > 0;
        }
    }
}