﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class TipoEventoController : ApiController
    {
        private TicketsContext db = new TicketsContext();

        // GET: api/TipoEvento
        public IQueryable<TipoEvento> GetTipoEvento()
        {
            return db.TipoEvento;
        }

        // GET: api/TipoEvento/5
        [ResponseType(typeof(TipoEvento))]
        public async Task<IHttpActionResult> GetTipoEvento(long id)
        {
            TipoEvento tipoEvento = await db.TipoEvento.FindAsync(id);
            if (tipoEvento == null)
            {
                return NotFound();
            }

            return Ok(tipoEvento);
        }

        // PUT: api/TipoEvento/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTipoEvento(long id, TipoEvento tipoEvento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoEvento.TipoEventoId)
            {
                return BadRequest();
            }

            db.Entry(tipoEvento).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoEventoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipoEvento
        [ResponseType(typeof(TipoEvento))]
        public async Task<IHttpActionResult> PostTipoEvento(TipoEvento tipoEvento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TipoEvento.Add(tipoEvento);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = tipoEvento.TipoEventoId }, tipoEvento);
        }

        // DELETE: api/TipoEvento/5
        [ResponseType(typeof(TipoEvento))]
        public async Task<IHttpActionResult> DeleteTipoEvento(long id)
        {
            TipoEvento tipoEvento = await db.TipoEvento.FindAsync(id);
            if (tipoEvento == null)
            {
                return NotFound();
            }

            db.TipoEvento.Remove(tipoEvento);
            await db.SaveChangesAsync();

            return Ok(tipoEvento);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TipoEventoExists(long id)
        {
            return db.TipoEvento.Count(e => e.TipoEventoId == id) > 0;
        }
    }
}