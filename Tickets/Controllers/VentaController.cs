﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class VentaController : ApiController
    {
        private TicketsContext db = new TicketsContext();

        // GET: api/Venta
        public IQueryable<Venta> GetVenta()
        {
            return db.Venta;
        }

        //GET: api/Venta/Mes/{NumeroMes}/{Anio}
        [HttpGet, Route("api/Venta/Mes/{NumeroMes}/{Anio}")]
        public IQueryable<Venta> getMonth(int NumeroMes, int Anio)
        {
            return (from vn in db.Venta where vn.Fecha.Month == NumeroMes && vn.Fecha.Year == Anio select vn);
        }

        //GET: api/Venta/Mes/{EventoId}
        [HttpGet, Route("api/Venta/MasVentas")]
        public Object getMasVentas()
        {
            Object datos = null;
            List<int> cuentas = new List<int>();
            List<String> nombres = new List<String>();
            foreach (Evento ev in db.Evento) {
                cuentas.Add((from dv in db.DetalleVenta where dv.EventoId == ev.EventoId select dv.DetalleVentaId).Count());
               }
            datos = new { cuenta = cuentas, NombreEvento = (from even in db.Evento select even.NombreEvento)};
            return datos;
        }

        //GET: api/Venta/Fecha/{Fecha}
        [HttpGet, Route("api/Venta/Fecha/{Fecha}")]
        public IQueryable<Venta> getFecha(String Fecha)
        {
            DateTime fecha = DateTime.Parse(Fecha);
           return (from vn in db.Venta where (vn.Fecha.Day == fecha.Day && vn.Fecha.Month == fecha.Month && vn.Fecha.Year == fecha.Year) select vn);
        }

     
        // GET: api/Venta/5
        [ResponseType(typeof(Venta))]
        public async Task<IHttpActionResult> GetVenta(long id)
        {
            Venta venta = await db.Venta.FindAsync(id);
            if (venta == null)
            {
                return NotFound();
            }

            return Ok(venta);
        }

        // PUT: api/Venta/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutVenta(long id, Venta venta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != venta.VentaId)
            {
                return BadRequest();
            }

            db.Entry(venta).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VentaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Venta
        [ResponseType(typeof(Venta))]
        public async Task<IHttpActionResult> PostVenta(Venta venta)
        {
            venta.Fecha = DateTime.Now;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Venta ven =db.Venta.Add(venta);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = ven.VentaId }, ven);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VentaExists(long id)
        {
            return db.Venta.Count(e => e.VentaId == id) > 0;
        }
    }
}