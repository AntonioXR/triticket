﻿using System;
using System.Web.Mvc;

namespace Tickets.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Triticket | Bienvenido";

            return View();
        }
    }
}
