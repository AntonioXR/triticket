﻿--------------------------------------------------------------------
				CARACTERISTICAS DE LOS WEB SERVICES
--------------------------------------------------------------------

			######## Creado 28/04/2017 ########
######### Especificaciones el dia antes especificado #########

		#			---- Rol (UsersType) ---
							-READ  


		#		--- TipoEVento (EventType) ---
			-CREATE	-READ  -UPDATE	-DELETE 


		#		--- TipoPersonal (PersonalType)---
			-CREATE	-READ  -UPDATE	-DELETE 


		#		--- PersonalEvento (EventPerson) ---
	-CREATE	-READ  -UPDATE	-DELETE		-READ FOR EVENT


		#		--- DetalleVenta (Sales Detail) ---
	-CREATE	-READ  -READ FOR SALE


		#		--- Usuario (User) ---
		-CREATE	-READ  -UPDATE	-DELETE -LOGIN -READ FOR ROL 


		#		--- Evento (Event) ---
	-CREATE	-READ  -UPDATE	-DELETE		 -READ FOR TYPE 
	-READ FOR DATE		-READ FOR PROMOTION


		#		--- Venta (Sales) ---
	-CREATE	-READ  -UPDATE(So so)	-READ FOR MONTH		
	-READ FOR EVENT		-READ FOR DAY


#################################################################################