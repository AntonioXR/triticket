﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;  
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class DetalleVentaController : ApiController
    {
        private TicketsContext db = new TicketsContext();

        // GET: api/DetalleVenta
        public IQueryable<DetalleVenta> GetDetalleVenta()
        {
            return db.DetalleVenta;
        }

        // GET: api/DetalleVenta/5
        [ResponseType(typeof(DetalleVenta))]
        public async Task<IHttpActionResult> GetDetalleVenta(long id)
        {
            DetalleVenta detalleVenta = await db.DetalleVenta.FindAsync(id);
            if (detalleVenta == null)
            {
                return NotFound();
            }

            return Ok(detalleVenta);
        }

      
        // POST: api/DetalleVenta
        [ResponseType(typeof(DetalleVenta))]
        public async Task<IHttpActionResult> PostDetalleVenta(DetalleVenta detalleVenta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Evento.Find(detalleVenta.EventoId).Existencias = db.Evento.Find(detalleVenta.EventoId).Existencias - 1;
            db.DetalleVenta.Add(detalleVenta);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = detalleVenta.DetalleVentaId }, detalleVenta);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //GET: api/DetalleVenta/Venta/{VentaId}
        [Route("api/DetalleVenta/Venta/{VentaId}")]
        public IQueryable<DetalleVenta> ShowDetalleVenta(int VentaId)
        {
            return (from dv in db.DetalleVenta where dv.VentaId== VentaId select dv);
        }

    }
}