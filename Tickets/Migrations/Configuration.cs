namespace Tickets.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Tickets.Models.TicketsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Tickets.Models.TicketsContext context)
        {
            context.Rol.AddOrUpdate(new Rol { RolId = 1, NombreRol = "Administrador" }, new Rol() { RolId = 2, NombreRol = "Usuario | Operador" }, new Rol() { RolId = 3, NombreRol = "Cliente" });
            context.Usuario.AddOrUpdate(new Usuario() { UsuarioId = 1, Nombre = "Antonio Xocoy Rosales", Email = "anxrosales@gmail.com", Nick = "tonioros", Contrasena = "123", Edad = 18, NoTarjeta = "2342343453453", RolId = 2, Telefono = 23483454, });
            context.TipoEvento.AddOrUpdate(new TipoEvento(1, "Concierto"), new TipoEvento(2, "Opera"), new TipoEvento(3, "Teatro"));
            context.TipoPersonal.AddOrUpdate(new TipoPersonal(1, "Cocinero"), new TipoPersonal(2, "Staff"));
            context.Evento.AddOrUpdate(new Evento() { EventoId = 1, NombreEvento = "Concierto Sinfonico", Existencias = 35, FechaEvento = DateTime.Now, Precio = 40.50, Lugar = "Ciudad de guatemala", Descripcion = "No c we ", Promociones = 0, TipoEventoId = 1, UsuarioId = 1 });
            context.PersonalEvento.AddOrUpdate(new PersonalEvento() { TipoPersonalId = 1, NombrePersona = "Mariana Cruz", PrecioPorHora = 150.10, EventoId = 1, PersonalEventoId = 1 });
        }
    }
}