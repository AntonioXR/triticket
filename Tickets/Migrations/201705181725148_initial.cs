namespace Tickets.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DetalleVentas",
                c => new
                    {
                        DetalleVentaId = c.Long(nullable: false, identity: true),
                        VentaId = c.Long(nullable: false),
                        EventoId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.DetalleVentaId)
                .ForeignKey("dbo.Eventoes", t => t.EventoId, cascadeDelete: true)
                .ForeignKey("dbo.Ventas", t => t.VentaId, cascadeDelete: true)
                .Index(t => t.VentaId)
                .Index(t => t.EventoId);
            
            CreateTable(
                "dbo.Eventoes",
                c => new
                    {
                        EventoId = c.Long(nullable: false, identity: true),
                        NombreEvento = c.String(),
                        Existencias = c.Int(nullable: false),
                        FechaEvento = c.DateTime(nullable: false),
                        Precio = c.Double(nullable: false),
                        Lugar = c.String(),
                        Descripcion = c.String(),
                        Promociones = c.Int(nullable: false),
                        TipoEventoId = c.Long(nullable: false),
                        UsuarioId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EventoId)
                .ForeignKey("dbo.TipoEventoes", t => t.TipoEventoId, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId, cascadeDelete: true)
                .Index(t => t.TipoEventoId)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.TipoEventoes",
                c => new
                    {
                        TipoEventoId = c.Long(nullable: false, identity: true),
                        TipoE = c.String(),
                    })
                .PrimaryKey(t => t.TipoEventoId);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        UsuarioId = c.Long(nullable: false, identity: true),
                        Nombre = c.String(),
                        Nick = c.String(),
                        Contrasena = c.String(),
                        Edad = c.Int(nullable: false),
                        Telefono = c.Long(nullable: false),
                        Email = c.String(),
                        NoTarjeta = c.String(),
                        RolId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.UsuarioId)
                .ForeignKey("dbo.Rols", t => t.RolId, cascadeDelete: true)
                .Index(t => t.RolId);
            
            CreateTable(
                "dbo.Rols",
                c => new
                    {
                        RolId = c.Long(nullable: false, identity: true),
                        NombreRol = c.String(),
                    })
                .PrimaryKey(t => t.RolId);
            
            CreateTable(
                "dbo.Ventas",
                c => new
                    {
                        VentaId = c.Long(nullable: false, identity: true),
                        VentaType = c.Int(nullable: false),
                        NombreComprador = c.String(),
                        NIT = c.String(),
                        Fecha = c.DateTime(nullable: false),
                        TotalVenta = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.VentaId);
            
            CreateTable(
                "dbo.PersonalEventoes",
                c => new
                    {
                        PersonalEventoId = c.Long(nullable: false, identity: true),
                        NombrePersona = c.String(),
                        PrecioPorHora = c.Double(nullable: false),
                        EventoId = c.Long(nullable: false),
                        TipoPersonalId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PersonalEventoId)
                .ForeignKey("dbo.Eventoes", t => t.EventoId, cascadeDelete: true)
                .ForeignKey("dbo.TipoPersonals", t => t.TipoPersonalId, cascadeDelete: true)
                .Index(t => t.EventoId)
                .Index(t => t.TipoPersonalId);
            
            CreateTable(
                "dbo.TipoPersonals",
                c => new
                    {
                        TipoPersonalId = c.Long(nullable: false, identity: true),
                        NombreTipoP = c.String(),
                    })
                .PrimaryKey(t => t.TipoPersonalId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PersonalEventoes", "TipoPersonalId", "dbo.TipoPersonals");
            DropForeignKey("dbo.PersonalEventoes", "EventoId", "dbo.Eventoes");
            DropForeignKey("dbo.DetalleVentas", "VentaId", "dbo.Ventas");
            DropForeignKey("dbo.DetalleVentas", "EventoId", "dbo.Eventoes");
            DropForeignKey("dbo.Eventoes", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.Usuarios", "RolId", "dbo.Rols");
            DropForeignKey("dbo.Eventoes", "TipoEventoId", "dbo.TipoEventoes");
            DropIndex("dbo.PersonalEventoes", new[] { "TipoPersonalId" });
            DropIndex("dbo.PersonalEventoes", new[] { "EventoId" });
            DropIndex("dbo.Usuarios", new[] { "RolId" });
            DropIndex("dbo.Eventoes", new[] { "UsuarioId" });
            DropIndex("dbo.Eventoes", new[] { "TipoEventoId" });
            DropIndex("dbo.DetalleVentas", new[] { "EventoId" });
            DropIndex("dbo.DetalleVentas", new[] { "VentaId" });
            DropTable("dbo.TipoPersonals");
            DropTable("dbo.PersonalEventoes");
            DropTable("dbo.Ventas");
            DropTable("dbo.Rols");
            DropTable("dbo.Usuarios");
            DropTable("dbo.TipoEventoes");
            DropTable("dbo.Eventoes");
            DropTable("dbo.DetalleVentas");
        }
    }
}
