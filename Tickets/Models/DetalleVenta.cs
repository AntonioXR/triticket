﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tickets.Models
{
    public class DetalleVenta
    {
        #region "Constructor"
        public DetalleVenta() { }
        public DetalleVenta(Int64 ID, Venta venta, Evento evento)
        {
            this.DetalleVentaId = ID;
            this.Venta = venta;
            this.VentaId = venta.VentaId;
            this.Evento = evento;
            this.EventoId = evento.EventoId;
        }
        #endregion
        #region "Propiedades"
        public Int64 DetalleVentaId { get; set; }

        public Venta Venta { get; set; }
        public Int64 VentaId { get; set; }

        public Evento Evento { get; set; }
        public Int64 EventoId { get; set; }
        #endregion
    }
}