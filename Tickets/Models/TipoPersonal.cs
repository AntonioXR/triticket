﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tickets.Models
{
    public class TipoPersonal
    {
        #region "Constructor"
        public TipoPersonal() { }
        public TipoPersonal(Int64 ID, String TP)
        {
            this.TipoPersonalId = ID;
            this.NombreTipoP = TP;
        }
        #endregion
        #region "Propiedades"
        public Int64 TipoPersonalId { get; set; }
        public String NombreTipoP { get; set; }
        #endregion
    }
}