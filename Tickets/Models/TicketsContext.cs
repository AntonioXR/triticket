﻿using System;
namespace Tickets.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class TicketsContext : DbContext
    {
        public TicketsContext()
            : base("name=TicketsContext")
        {
        }
        public DbSet<DetalleVenta> DetalleVenta { get; set; }
        public DbSet<TipoEvento> TipoEvento { get; set; }
        public DbSet<TipoPersonal> TipoPersonal { get; set; }
        public DbSet<PersonalEvento> PersonalEvento { get; set; }
        public DbSet<Rol> Rol { get; set; }
        public DbSet<Venta> Venta { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Evento> Evento { get; set; }
    }
}