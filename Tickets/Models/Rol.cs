﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tickets.Models
{
    public class Rol
    {
        #region "Constructores"
        public Rol() {
        }

        #endregion
        #region "Propiedades"
        public Int64 RolId { get; set; }
        public String NombreRol { get; set; }
        #endregion
    }
}