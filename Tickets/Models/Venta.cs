﻿using System;
using System.ComponentModel.DataAnnotations;
namespace Tickets.Models
{
    public class Venta
    {
        #region "Constructores"
        public Venta() { }
        #endregion
        #region "Propiedades"
        public Int64 VentaId { get; set; }
        public int VentaType { get; set; }

        public String NombreComprador { get; set; }
        public String NIT { get; set; }

        public DateTime Fecha { get; set; }
        public Double TotalVenta { get; set; }
        #endregion
    }
}