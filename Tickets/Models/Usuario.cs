﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tickets.Models
{
    public class Usuario
    {
        #region "Constructores"
        public Usuario() { }
        public Usuario(Int64 ID, String Nombre, String Nick, String Contrasena, int Edad, Int64 Telefono, String Email, String NoTarjeta, Rol rol) {
            this.UsuarioId = ID;
            this.Nombre = Nombre;
            this.Nick = Nick;
            this.Contrasena = Contrasena;
            this.Edad = Edad;
            this.Telefono = Telefono;
            this.Email = Email;
            this.NoTarjeta = NoTarjeta;
            this.Rol = rol;
            this.RolId = rol.RolId;
        }
        #endregion
        #region "Propiedades"
        public Int64 UsuarioId { get; set; }
        public String Nombre { get; set; }
        public String Nick { get; set; }
        public String Contrasena { get; set; }
        public int Edad { get; set; }
        public Int64 Telefono { get; set; }
        public String Email { get; set; }
        public String NoTarjeta { get; set; }

        public Int64 RolId { get; set; }
        public Rol Rol { get; set; }

        #endregion
    }
}