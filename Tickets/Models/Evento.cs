﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tickets.Models
{
    public class Evento
    {
        #region "Constructor"
        public Evento() { }
        public Evento(Int64 ID, String nombre, int existencias, DateTime fechaE, Double precio, TipoEvento tipoEvento, String lugar, String descrip, int Promosion, Usuario user)
        {
            this.EventoId = ID;
            this.NombreEvento = nombre;
            this.Existencias = existencias;
            this.FechaEvento = fechaE;
            this.Precio = precio;
            this.TipoEvento = tipoEvento;
            this.Lugar = lugar;
            this.Descripcion = descrip;
            this.Usuario = user;
            this.TipoEventoId = tipoEvento.TipoEventoId;
            this.UsuarioId = user.UsuarioId;
            this.Promociones = Promosion;
        }
        #endregion
        #region "Propiedades"
        public Int64 EventoId { get; set; }
        public String NombreEvento { get; set; }
        public int Existencias { get; set; }
        public DateTime FechaEvento { get; set; }
        public Double Precio { get; set; }
        public String Lugar { get; set; }
        public String Descripcion { get; set; }
        public int Promociones { get; set; }

        public Int64 TipoEventoId { get; set; }
        public TipoEvento TipoEvento { get; set; }

        public Int64 UsuarioId { get; set; }
        public Usuario Usuario { get; set; }
        #endregion
    }
}