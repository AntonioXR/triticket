﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tickets.Models
{
    public class TipoEvento
    {
        #region "Constructor"
        public TipoEvento() { }
        public TipoEvento(Int64 ID, String Tipo) {
            this.TipoEventoId = ID;
            this.TipoE = Tipo;
        }
        #endregion
        #region "Propiedades"
        public Int64 TipoEventoId { get; set; }
        public String TipoE { get; set; }
        #endregion
    }
}