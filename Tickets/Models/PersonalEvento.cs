﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tickets.Models
{
    public class PersonalEvento
    {
        #region "Constructor"
        public PersonalEvento() { }

        #endregion
        #region "Propiedades"
        public Int64 PersonalEventoId { get; set; }
        public String NombrePersona { get; set; }
        public Double PrecioPorHora { get; set; }

        public Evento Evento { get; set; }
        public Int64 EventoId { get; set; }

        public TipoPersonal TipoPersonal{get;set;}
        public Int64 TipoPersonalId { get; set; }
        #endregion 
    }
}